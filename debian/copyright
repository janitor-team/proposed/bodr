Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: BODR
Upstream-Contact: https://github.com/egonw/bodr/issues
Source: https://github.com/egonw/bodr
Comment: This package was debianized by Egon Willighagen. It is currently
 maintained by the debichem team <debichem-devel@lists.alioth.debian.org>.
Disclaimer: Please cite this article if you find this program useful:
 Guha, R., Howard, M. T., Hutchison, G. R., Murray-Rust, P., Rzepa, H.,
 Steinbeck, C., Wegner, J., Willighagen, E. L., May 2006. The Blue Obelisk -
 interoperability in chemical informatics. J. Chem. Inf. Model. 46 (3), 991-998.
 .
 https://dx.doi.org/10.1021/ci050400b

Files: *
Copyright: 2006-2010 The Blue Obelisk Project
License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to
 deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 IN THE SOFTWARE.

Files: bibtexml-xslt/*.xsl
Copyright: 2006 Vidar Bronken Gundersen, Zeger W. Hendrikse
Comment: The files were originally downloaded from <https://bibtexml.sf.net>.
License: GPL-2
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, see <https://www.gnu.org/licenses/>
 or '/usr/share/common-licenses/GPL-2', or write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

Files: debian/*
Copyright: 2007 Egon Willighagen <e.willighagen@science.ru.nl>
           2007-2019 The debichem team <debichem-devel@lists.alioth.debian.org>
License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, see <https://www.gnu.org/licenses/>
 or '/usr/share/common-licenses/GPL-2', or write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
